import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColParser {

	private boolean[][] adjacencyMatrix;

	public ColParser(String filename) {
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			Pattern pattern;
			Matcher matcher;
			String temp;
			
			do {
				temp = br.readLine();
				pattern = Pattern.compile("^p edge (\\d+) \\d+$");
				matcher = pattern.matcher(temp);
			} while (!matcher.find());
			
			int verticesNumber = Integer.parseInt(matcher.group(1));
			adjacencyMatrix = new boolean[verticesNumber][verticesNumber];

			pattern = Pattern.compile("^e (\\d+) (\\d+)$");
			temp = br.readLine();
			while (temp != null) {
				matcher = pattern.matcher(temp);
				matcher.find();
				int firstIndex = Integer.parseInt(matcher.group(1)) - 1;
				int secondIndex = Integer.parseInt(matcher.group(2)) - 1;
				adjacencyMatrix[firstIndex][secondIndex] = true;
				adjacencyMatrix[secondIndex][firstIndex] = true;

				temp = br.readLine();
			}
			
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println("Something went wrong");
		}
	}

	public boolean[][] getAdjacencyMatrix() {
		return adjacencyMatrix;
	}
}
